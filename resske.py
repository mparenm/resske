#!/usr/bin/env python

# Michael Moses <mparenm@gmail.com>
#
# This is a script to create symlinks to the .ssh directory in a users home
# directory to and .ssh directory on an external drive. This allows the user
# to keep their keys on an external drive without having to alter their ssh
# connection properties. When the drive is no longer mounted, the config
# file is no longer available, but the keys are still stored in the
# ssh-agent until they are released.


import os
from os import curdir
from os import chdir
from os.path import isfile
from os.path import isdir
from os.path import join
from os.path import abspath
from os.path import islink
import sys

__version__ = "0.4"
__name = "resske"
CONFIG_FILE = join(sys.path[0], 'config')


def main(args, **kwargs):
    config = get_config()
    origin = get_origin(config['drives'])
    ssh_dir = get_ssh_dir(get_home())
    message = None

    if ssh_dir is None:
        message = "Unable to build keys, could not find ssh directory in "
        "home path."
    elif origin is None:
        message = "Unable to build keys. None of the volumes in locations "
        "sections of the config file are mounted."
    if message is not None:
        die(message)
    rebuild_keys(ssh_dir, origin, config['keys'])

def get_home():
    '''
    Return the home environment of the user
    '''
    return os.getenv('HOME')

def get_locations():
    '''
    FFD
    Returns a list of possible external drives to be searched for an .ssh dir
    with a valid set of key files. Primarily so that the script does not
    require editing if I forget my primary usb drive.
    '''
    return  [
        "/media/marc",
        "/media/starc"]

def get_config():
    from ConfigParser import ConfigParser
    parser = ConfigParser()

    config = {
        'drives' : [],
        'keys' : []}

    try:
        parser.read(CONFIG_FILE)
    except IOError:
        die('Config file not found')

    try:
        config['drives'] = parser.get('locations', 'drives')\
            .replace(' ', '').split(',')
        config['keys'] = parser.get('files', 'keys')\
            .replace(' ', '').split(',')
    except Exception as err:
        errmsg = [
            "resske could not load the config file successfully",
            "Here's the error mesasge that was returned from the "
            "configparser library:",
            "%s " % (err)]

        die(errmsg)
    return config

def remove_key(f):
    '''
    Deletes the existing key file
    @PARAM - Full path to the file
    '''
    if len(f) > 5 and islink(f):
        try:
            os.remove(f)
        except Exception as err:
            print(err)
            sys.exit()

def create_key(origin, f):
    '''
    Creates the symlink for the key file
    @PARAM - origin path for the .ssh key to copy
    @PARAM - path to create the symlink to
    '''
    os.symlink(join(origin, get_filename(f)), f)

def get_origin(locations):
    '''
    Takes a list of possible external drives and checks each for an .ssh
    directory. This allows .ssh keys to exist on multiple external drives
    without the need to edit the script on a regular basis.
    '''
    for loc in locations:
        origin = join(loc, '.ssh')
        if isdir(origin):
            return origin

def is_mounted(dir):
    '''
    Test the drive to see if it is mounted
    @PARAM - Takes an arugment for a drive path
    '''
    if os.path.isdir(dir):
        return True
    return False

def get_ssh_dir(home):
    ssh_dir = join(home, '.ssh')
    if isdir(ssh_dir):
        return ssh_dir
    return None

def get_filename(path):
    return path.split('/')[-1]

def rebuild_keys(ssh_dir, origin, keys):
    for key in keys:
        key = join(ssh_dir, key)
        if islink(key):
            remove_key(key)
        create_key(origin, join(ssh_dir, key))

def die(message):
    from sys import exit
    print('')
    if isinstance(message, str):
        print(message)
    else:
        print('\n'.join(message))
    print('')
    exit()

def _build_options(args):
    import argparse

    parser = argparse.ArgumentParser(description=_description())
    parser.add_argument('-a', action="store_true", default=False)

def _description():
    '''
    Returns the description for the program
    '''
    desc = " %s - A program to symlink SSH keys on an external drive to a "
    "user'shome directory" % (__name)

    return desc

if __name__ == '__main__':
    main(sys.argv)
